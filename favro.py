class Favro:
    def __init__(self, organisation_id: str, username: str, api_token: str, card_prefix: str):
        self.organisation_id = organisation_id
        self.username = username
        self.api_token = api_token
        self.card_prefix = card_prefix


    def get_organisation_url(self):
        return 'https://favro.com/organization/' + self.organisation_id


    def get_card_url(self, card_id: str):
        return '{}?card={}{}'.format(self.get_organisation_url(), self.card_prefix, card_id)