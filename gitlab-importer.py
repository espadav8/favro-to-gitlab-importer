from http import HTTPStatus

import argparse
import json
import os
import re
import sys
import urllib.parse
import urllib.request

from colorama import Fore, Back, Style
from favro import Favro


filename = 'favro-cards-export.json'
gitlab_access_token = os.environ['GITLAB_ACCESS_TOKEN']
gitlab_project_id = os.environ['GITLAB_PROJECT_ID']
gitlab_project_url = 'https://gitlab.com/api/v4/projects/' + gitlab_project_id


with open('assignee_map.json') as assignees:
    assignee_map = json.load(assignees)


with open('milestone_map.json') as milestones:
    milestone_map = json.load(milestones)


def send_gitlab_request(url: str, method: str = 'GET', data=None):
    request = urllib.request.Request(url, method=method, data=data)
    request.add_header('PRIVATE-TOKEN', gitlab_access_token)

    return urllib.request.urlopen(request)


def clear_issues():
    print(Fore.RED + 'DELETING ALL EXISTING ISSUES' + Style.RESET_ALL)

    while True:
        response = get_existing_issues()
        issues = json.load(response)

        if len(issues) == 0:
            break

        for issue in issues:
            iid = issue['iid']
            name = issue['title']
            print(Fore.YELLOW + 'Deleting: {} - {}'.format(iid, name) + Style.RESET_ALL)

            try:
                send_gitlab_request(
                    '{}/issues/{}'.format(gitlab_project_url, iid),
                    'DELETE'
                )
            except urllib.error.HTTPError as e:
                print('Got a 404, assuming no more issues exist')
                return


def get_existing_issues():
    response = send_gitlab_request(
        gitlab_project_url + '/issues',
    )

    return response


def start_import():
    with open(filename) as f:
        favro = json.load(f)

        for card in favro['rows']:
            iid = get_iid(card['Card Id'])
            name = get_name(card.get('Name', ''))

            if already_exists(iid):
                print(Fore.CYAN + 'Skipping: {} - {}'.format(iid, name) + Style.RESET_ALL)
                continue

            print(Fore.GREEN + 'Creating: {}'.format(name) + Style.RESET_ALL)

            description = get_description(card.get('Description', ''), iid)
            labels = get_labels(card.get('Tags', []), card.get('Members', []))
            weight = get_weight(card.get('Estimation', ''))
            created_at = card.get('Creation date', '')
            assignees = get_assignees(card.get('Members', []))
            milestone = get_milestone_id(card.get('Tags', []))
            closed = should_close(card.get('Tags', []))

            data = urllib.parse.urlencode({
                'assignee_ids[]': assignees,
                'created_at': created_at,
                'description': description,
                'iid': iid,
                'labels[]': labels,
                'milestone_id': milestone,
                'title': name,
                'weight': weight,
            }, True)
            data = data.encode('ascii')

            try:
                send_gitlab_request(gitlab_project_url + '/issues', method='POST', data=data)
            except urllib.error.HTTPError as e:
                print(Fore.RED)
                print(e)
                print(Fore.MAGENTA)
                print(data)
                print(Style.RESET_ALL)
                continue

            if closed:
                data = urllib.parse.urlencode({
                    'state_event': 'close'
                }, True)
                data = data.encode('ascii')

                send_gitlab_request(
                    gitlab_project_url + '/issues/' + iid,
                    method='PUT',
                    data=data
                )

    return


def get_iid(card_id: str):
    return card_id[len(favro.card_prefix):]


def already_exists(iid: str):
    try:
        response = send_gitlab_request(gitlab_project_url + '/issues/' + iid)
    except urllib.error.HTTPError as e:
        if e.code == HTTPStatus.NOT_FOUND:
            return False

    return response.status == HTTPStatus.OK


def get_name(name: str):
    return name.strip()


def get_weight(estimation: str):
    if estimation == '':
        return ''

    return estimation.replace(' pts', '')


def get_description(description: str, iid: str):
    description = re.sub(r"[ ]+", ' ', description)
    description = description.replace('\n', '\n\n')
    description = description.replace('\n☐ ', '- [ ] ')
    description = description.replace('\n☑ ', '- [x] ')
    description = description.replace('\n‣ ', '1. ')
    description = description.replace('\n• ', '* ')

    description += '\n\nimported from ' + favro.get_card_url(iid)

    return description


def get_labels(tags: list, members: list):
    labels = []

    for tag in tags:
        labels.append(tag.strip())

    for member in members:
        if member not in assignee_map:
            labels.append(member)

    return labels


def get_assignees(members: dict):
    assignee_ids = []

    for member in members:
        if member in assignee_map:
            assignee = assignee_map.get(member)

            if type(assignee) is int:
                assignee_ids.append(assignee)
            elif type(assignee) is list:
                assignee_ids.extend(assignee)

    return list(set(assignee_ids))


def get_milestone_id(tags: list):
    for tag in tags:
        if tag in milestone_map:
            return milestone_map[tag]

    return ''


def should_close(tags: list):
    closed_milestones = [
        'Kiki 0',
        'Kiki 1',
        'Kiki 2',
        'Kiki 3',
        'Kiki 4',
        'Kiki 5',
        'Kiki 6',
        'Track 1',
        'Track 2',
        'Track 3',
        'Track 4',
        'Track 5',
        'Track 6',
    ]

    intersect = list(set(tags) & set(closed_milestones))

    return len(intersect) > 0


def create_favro():
    api_token = os.environ['FAVRO_API_TOKEN']
    card_prefix = os.environ['FAVRO_CARD_PREFIX']
    organisation_id = os.environ['FAVRO_ORGANISATION']
    username = os.environ['FAVRO_ORGANISATION']

    return Favro(organisation_id, username, api_token, card_prefix)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', type=str, required=False)
    parser.add_argument('--clear', action="store_true", required=False)
    args = parser.parse_args()

    if args.clear:
        clear_issues()

    favro = create_favro()
    start_import()
