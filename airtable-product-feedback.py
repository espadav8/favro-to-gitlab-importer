from http import HTTPStatus

import json
from csv import DictReader
import os
import re
import sys
import urllib.parse
import urllib.request


filename = 'airtable-product-feedback.csv'
gitlab_access_token = os.environ['GITLAB_ACCESS_TOKEN']
gitlab_project_id = '20323914'
gitlab_project_url = 'https://gitlab.com/api/v4/projects/' + gitlab_project_id


def send_gitlab_request(url: str, method: str = 'GET', data=None):
    request = urllib.request.Request(url, method=method, data=data)
    request.add_header('PRIVATE-TOKEN', gitlab_access_token)

    return urllib.request.urlopen(request)


def clear_issues():
    print('DELETING ALL EXISTING ISSUES')

    while True:
        response = get_existing_issues()
        issues = json.load(response)

        if len(issues) == 0:
            break

        for issue in issues:
            iid = issue['iid']
            print('Deleting: {}'.format(iid))

            try:
                send_gitlab_request(
                    '{}/issues/{}'.format(gitlab_project_url, iid),
                    'DELETE'
                )
            except urllib.error.HTTPError as e:
                print('Got a 404, assuming no more issues exist')
                return


def get_existing_issues():
    response = send_gitlab_request(
        gitlab_project_url + '/issues',
    )

    return response


def start_import():
    template_file = open('product-feedback-template.md')
    description_template = template_file.read()
    template_file.close()

    with open(filename, newline='') as f:
        data = DictReader(f)

        for row in data:
            iid = row['id']
            name = get_name(row.get('Product Title', ''))

            if already_exists(iid):
                print('Skipping: ' + name)
                continue

            print('Creating: ' + name)

            description = get_description(row, description_template)
            labels = get_labels(row)
            closed = should_close(row)

            data = urllib.parse.urlencode({
                # 'assignee_ids': assignees,
                'description': description,
                'iid': iid,
                'labels[]': labels,
                'title': name,
            }, True)
            data = data.encode('ascii')

            send_gitlab_request(gitlab_project_url + '/issues', method='POST', data=data)

            if closed:
                data = urllib.parse.urlencode({
                    'state_event': 'close'
                }, True)
                data = data.encode('ascii')

                send_gitlab_request(
                    gitlab_project_url + '/issues/' + iid,
                    method='PUT',
                    data=data
                )

    return


def already_exists(iid: str):
    try:
        response = send_gitlab_request(gitlab_project_url + '/issues/' + iid)
    except urllib.error.HTTPError as e:
        if e.code == HTTPStatus.NOT_FOUND:
            return False

    return response.status == HTTPStatus.OK


def get_name(name: str):
    return name.strip()


def get_description(row: list, template: str):
    description = template \
        .replace('{{ problem-statement }}', row['Problem Statement']) \
        .replace('{{ proposed-solution }}', row['Proposed Solution']) \
        .replace('{{ customer-reported }}', row['Customer Reported']) \
        .replace('{{ prospect-opportunity }}', row['Prospect Opportunity']) \
        .replace('{{ name }}', row['Your Name']) \
        .replace('{{ email }}', row['Reporter Email'])

    return description


def get_labels(row: list):
    tenants = row.get('Customer Reported', '')
    domain = row.get('Domain', '')
    status = row.get('Status', '')

    labels = [
        'Product Feedback'
    ]

    if domain != '':
        labels.append('Domain::' + domain)

    if status != '':
        labels.append('Status::' + status)

    if tenants != '':
        labels.extend(map(lambda x: 'Customer:' + x, tenants.split(',')))

    return labels


def should_close(row: list):
    closed_status = [
        'Released',
        "Won't Do",
    ]

    return row.get('Status', '') in closed_status


if __name__ == "__main__":
    if '--clear' in sys.argv:
        clear_issues()

    start_import()
