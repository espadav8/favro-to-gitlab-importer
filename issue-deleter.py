from http import HTTPStatus

import json
import os
import re
import sys
import urllib.parse
import urllib.request


gitlab_access_token = os.environ['GITLAB_ACCESS_TOKEN']
gitlab_project_id = os.environ['GITLAB_PROJECT_ID']
gitlab_project_url = 'https://gitlab.com/api/v4/projects/' + gitlab_project_id

def send_gitlab_request(url: str, method: str = 'GET', data=None):
    request = urllib.request.Request(url, method=method, data=data)
    request.add_header('PRIVATE-TOKEN', gitlab_access_token)

    return urllib.request.urlopen(request)


def clear_issues():
    print('DELETING CLOSED EXISTING ISSUES')

    while True:
        response = get_closed_issues()
        issues = json.load(response)

        if len(issues) == 0:
            break

        for issue in issues:
            iid = issue['iid']
            print('Deleting: {}'.format(iid))

            try:
                send_gitlab_request(
                    '{}/issues/{}'.format(gitlab_project_url, iid),
                    'DELETE'
                )
            except urllib.error.HTTPError as e:
                print('Got a 404, assuming no more issues exist')
                return


def get_closed_issues():
    response = send_gitlab_request(
        gitlab_project_url + '/issues?state=closed',
    )

    return response


if __name__ == "__main__":
    clear_issues()
