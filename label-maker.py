from http import HTTPStatus

import argparse
import json
import os
import re
import sys
import urllib.parse
import urllib.request

from colorama import Fore, Back, Style
from favro import Favro

filename = 'favro-tags-page-0.json'
gitlab_access_token = os.environ['GITLAB_ACCESS_TOKEN']
gitlab_project_id = os.environ['GITLAB_PROJECT_ID']
gitlab_project_url = 'https://gitlab.com/api/v4/groups/' + gitlab_project_id


with open('colour_map.json') as colours:
    colour_map = json.load(colours)


def send_gitlab_request(url: str, method: str = 'GET', data=None):
    request = urllib.request.Request(url, method=method, data=data)
    request.add_header('PRIVATE-TOKEN', gitlab_access_token)

    return urllib.request.urlopen(request)


def clear_labels():
    print(Fore.RED + 'DELETING ALL EXISTING LABELS' + Style.RESET_ALL)

    while True:
        response = get_existing_labels()
        labels = json.load(response)

        if len(labels) == 0:
            break

        for label in labels:
            id = label['id']
            name = label['name'].strip()
            description = label['description'].strip()

            if description is not 'Imported from Favro':
                print(Fore.RED + 'Not deleting: {}'.format(name) + Style.RESET_ALL)
                continue

            print(Fore.YELLOW + 'Deleting: {}'.format(name) + Style.RESET_ALL)

            try:
                send_gitlab_request(
                    '{}/labels/{}'.format(gitlab_project_url, id),
                    'DELETE'
                )
            except urllib.error.HTTPError as e:
                print(Fore.RED + 'Got a 404, assuming no more labels exist' + Style.RESET_ALL)
                return


def get_existing_labels():
    response = send_gitlab_request(
        gitlab_project_url + '/labels',
    )

    return response


def start_import():
    url = gitlab_project_url + '/labels'

    with open(filename) as f:
        favro = json.load(f)

        for label in favro['entities']:
            name = label['name'].strip()

            colour = get_colour(label['color'])

            data = urllib.parse.urlencode({
                'color': colour,
                'description': 'Imported from Favro',
                'name': name,
            }, True, safe='#')
            data = data.encode('ascii')

            try:
                send_gitlab_request(url, method='POST', data=data)
            except urllib.error.HTTPError as e:
                if e.code == HTTPStatus.CONFLICT:
                    print(Fore.RED + 'Conflict with: {}'.format(name) + Style.RESET_ALL)
                    continue
                else:
                    print(Fore.RED)
                    print(e)
                    print(data)
                    print(Style.RESET_ALL)
                    sys.exit(1)

            print(Fore.GREEN + 'Created: {}'.format(name) + Style.RESET_ALL)

    return


def get_colour(colour: str):
    if colour in colour_map:
        return colour_map.get(colour)

    return '#428BCA'


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--clear', action="store_true", required=False)
    args = parser.parse_args()

    if args.clear:
        clear_labels()


    for i in range(0, 5):
        filename = 'favro-tags-page-{}.json'.format(i)
        print(Fore.WHITE)
        print('--------------------------------------------------')
        print('Importing file {}'.format(filename))
        print('--------------------------------------------------')
        print(Style.RESET_ALL)
        start_import()
